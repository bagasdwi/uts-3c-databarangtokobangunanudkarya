package dwi.bagas.tokobangunanudkarya

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class CustomAdapter(val context : Context,arrayList: ArrayList<HashMap<String, Any>>): BaseAdapter() {
    val F_ID_BARANG   = "id_barang"
    val F_NAMA_BARANG = "nama_barang"
    val F_STOK        = "stok"
    val F_HARGA       = "harga"
    val F_NAMA_GAMBAR = "nama_gambar"
    val F_TIPE_GAMBAR = "tipe_gambar"
    val F_URL_GAMBAR  = "url_gambar"
    val list          = arrayList
    var uri           = Uri.EMPTY

    inner class ViewHolder() {
        var txIdBarang: TextView? = null
        var txNamaBarang: TextView? = null
        var txStok: TextView? = null
        var txHarga: TextView? = null
        var txNamaGambar: TextView? = null
        var txTipeGambar: TextView? = null
        var txUrlGambar: TextView? = null
        var imv: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view: View? = convertView
        if (convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_data_barang, null, true)

            holder.txIdBarang = view!!.findViewById(R.id.txIdBarang) as TextView
            holder.txNamaBarang = view!!.findViewById(R.id.txNamaBarang) as TextView
            holder.txStok = view!!.findViewById(R.id.txStok) as TextView
            holder.txHarga = view!!.findViewById(R.id.txHarga) as TextView
            holder.txNamaGambar = view!!.findViewById(R.id.txNamaGambar) as TextView
            holder.txTipeGambar = view!!.findViewById(R.id.txTipeGambar) as TextView
            holder.txUrlGambar = view!!.findViewById(R.id.txUrlGambar) as TextView
            holder.imv = view!!.findViewById(R.id.imageView) as ImageView

            view.tag = holder
        } else {
            holder = view!!.tag as ViewHolder
        }

        var TipeGambar: String = list.get(position).get(F_TIPE_GAMBAR).toString()
        uri = Uri.parse(list.get(position).get(F_URL_GAMBAR).toString())

        holder.txNamaGambar!!.setText(list.get(position).get(F_NAMA_GAMBAR).toString())
        holder.txIdBarang!!.setText(list.get(position).get(F_ID_BARANG).toString())
        holder.txNamaBarang!!.setText(list.get(position).get(F_NAMA_BARANG).toString())
        holder.txStok!!.setText(list.get(position).get(F_STOK).toString())
        holder.txHarga!!.setText(list.get(position).get(F_HARGA).toString())
        holder.txTipeGambar!!.setText(TipeGambar)
        holder.txUrlGambar!!.setText(uri.toString())
        holder.txUrlGambar!!.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW).setData(
                Uri.parse(holder.txUrlGambar!!.text.toString())
            )

            context.startActivity(intent)
        }

        when (TipeGambar) {
            ".jpg" -> {
                Picasso.get().load(uri).into(holder.imv)
            }
        }

        return view!!

    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }
}



