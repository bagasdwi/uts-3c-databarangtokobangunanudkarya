package dwi.bagas.tokobangunanudkarya

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_data_barang.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class BarangActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alBarang : ArrayList<HashMap<String,Any>>
    lateinit var adapter: CustomAdapter
    lateinit var uri : Uri
    val F_ID_BARANG     = "id_barang"
    val F_NAMA_BARANG   = "nama_barang"
    val F_STOK          = "stok"
    val F_HARGA         = "harga"
    val F_NAMA_GAMBAR   = "nama_gambar"
    val F_TIPE_GAMBAR   = "tipe_gambar"
    val F_URL_GAMBAR    = "url_gambar"
    val RC_OK           = 100
    var TipeGambar      =""
    var NamaGambar      =""
    var docId           = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_barang)

        btnUbah.setOnClickListener(this)
        btnUpImg.setOnClickListener(this)
        btnTambah.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        lsV.setOnItemClickListener(itemClick)
        alBarang = ArrayList()
        uri = Uri.EMPTY
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alBarang.get(position)
        NamaGambar = hm.get(F_NAMA_GAMBAR).toString()
        TipeGambar = hm.get(F_TIPE_GAMBAR).toString()
        docId = hm.get(F_ID_BARANG).toString()
        edIdbarang.setText(docId)
        edNamabarang.setText(hm.get(F_NAMA_BARANG).toString())
        edStokbarang.setText(hm.get(F_STOK).toString())
        edHarga.setText(hm.get(F_HARGA).toString())
        Toast.makeText(this,"Gambar $NamaGambar$TipeGambar yang dipilih",Toast.LENGTH_SHORT).show()
        txSelectedFile.setText("Gambar $NamaGambar$TipeGambar yang dipilih")
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("barang")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException!=null){
                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
            }
            showData()
        }
    }

    fun showData(){
        db.get().addOnSuccessListener { result ->
            alBarang.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(F_TIPE_GAMBAR, doc.get(F_TIPE_GAMBAR).toString())
                hm.put(F_NAMA_GAMBAR, doc.get(F_NAMA_GAMBAR).toString())
                hm.put(F_ID_BARANG, doc.get(F_ID_BARANG).toString())
                hm.put(F_NAMA_BARANG, doc.get(F_NAMA_BARANG).toString())
                hm.put(F_STOK, doc.get(F_STOK).toString())
                hm.put(F_HARGA, doc.get(F_HARGA).toString())
                hm.put(F_URL_GAMBAR, doc.get(F_URL_GAMBAR).toString())
                alBarang.add(hm)
            }
            adapter = CustomAdapter(this,alBarang)
            lsV.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
                txSelectedFile.setText(uri.toString())
            }
        }
    }

    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnUpImg ->{
                TipeGambar = ".jpg"
                intent.setType("image/*")}
            R.id.btnTambah ->{
                if (uri !=null){
                    NamaGambar = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(NamaGambar+TipeGambar)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            val  hm = HashMap<String,Any>()
                            hm.put(F_NAMA_GAMBAR, NamaGambar)
                            hm.put(F_TIPE_GAMBAR,TipeGambar)
                            hm.set(F_ID_BARANG,edIdbarang.text.toString())
                            hm.set(F_NAMA_BARANG,edNamabarang.text.toString())
                            hm.set(F_STOK,edStokbarang.text.toString())
                            hm.set(F_HARGA,edHarga.text.toString())
                            hm.put(F_URL_GAMBAR, taks.result.toString())
                            db.document(NamaGambar).set(hm).addOnSuccessListener {
                                Toast.makeText(
                                    this,
                                    "Beshasil menambahkan data barang",
                                    Toast.LENGTH_SHORT
                                ).show()
                                txSelectedFile.setText("Tidak ada gambar dipilih")
                            }
                        }
                }
            }
            R.id.btnUbah ->{
                val hm = HashMap<String, Any>()
                hm.put(F_NAMA_GAMBAR, NamaGambar)
                hm.put(F_TIPE_GAMBAR,TipeGambar)
                hm.set(F_ID_BARANG, edIdbarang.text.toString())
                hm.set(F_NAMA_BARANG, edNamabarang.text.toString())
                hm.set(F_STOK, edStokbarang.text.toString())
                hm.set(F_HARGA, edHarga.text.toString())
                db.document(NamaGambar).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Berhasil mengubah data barang", Toast.LENGTH_SHORT)
                            .show()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(
                            this,
                            "Gagal mengubah data : ${e.message}",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
            }
            R.id.btnHapus ->{
                val fileRef = storage.child(NamaGambar+TipeGambar)
                fileRef.delete().addOnSuccessListener {

                }.addOnFailureListener {

                }
                db.whereEqualTo(F_NAMA_GAMBAR,NamaGambar).get()
                    .addOnSuccessListener {results ->
                        for (doc in results){
                            db.document(doc.id).delete()
                                .addOnSuccessListener {
                                    Toast.makeText(this,"Berhasil menghapus data barang",Toast.LENGTH_SHORT)
                                        .show()
                                    txSelectedFile.setText("Tidak ada file dipilih")
                                }
                                .addOnFailureListener { e ->
                                    Toast.makeText(this,"Gagal menghapus data barang : ${e.message}",Toast.LENGTH_SHORT)
                                        .show() }
                        }
                    }.addOnFailureListener { e ->
                        Toast.makeText(this,"Tidak bisa menemukan referensi : ${e.message}",Toast.LENGTH_SHORT)
                            .show() }
            }
        }
        if(!(v?.id == R.id.btnTambah || v?.id == R.id.btnUbah || v?.id == R.id.btnHapus)) startActivityForResult(intent,RC_OK)
    }
}