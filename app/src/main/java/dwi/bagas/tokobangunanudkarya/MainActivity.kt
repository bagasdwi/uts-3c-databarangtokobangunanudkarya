package dwi.bagas.tokobangunanudkarya

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin ->{
                var login = Intent(this, LoginActivity::class.java)
                startActivity(login)
                true
            }

            R.id.btnLokasiToko ->{
                var lokasiToko = Intent(this, LokasiTokoActivity::class.java)
                startActivity(lokasiToko)
                true
            }
            R.id.btnBarang ->{
                var cariBarang = Intent(this, CariBarangActivity::class.java)
                startActivity(cariBarang)
                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        btnLokasiToko.setOnClickListener(this)
        btnBarang.setOnClickListener(this)
    }


}
