package dwi.bagas.tokobangunanudkarya

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_cari_barang.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CariBarangActivity : AppCompatActivity() {
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alBarang : ArrayList<HashMap<String,Any>>
    lateinit var adapter: CustomAdapter
    lateinit var uri : Uri
    val F_ID_BARANG     = "id_barang"
    val F_NAMA_BARANG   = "nama_barang"
    val F_STOK          = "stok"
    val F_HARGA         = "harga"
    val F_NAMA_GAMBAR   = "nama_gambar"
    val F_TIPE_GAMBAR   = "tipe_gambar"
    val F_URL_GAMBAR    = "url_gambar"
    val RC_OK           = 100
    var TipeGambar      =""
    var NamaGambar      =""
    var docId           = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cari_barang)


        lsDataCariBarang.setOnItemClickListener(itemClick)
        alBarang = ArrayList()
        uri = Uri.EMPTY
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alBarang.get(position)
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("barang")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException!=null){
                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
            }
            showData()
        }
    }

    fun showData(){
        db.get().addOnSuccessListener { result ->
            alBarang.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(F_TIPE_GAMBAR, doc.get(F_TIPE_GAMBAR).toString())
                hm.put(F_NAMA_GAMBAR, doc.get(F_NAMA_GAMBAR).toString())
                hm.put(F_ID_BARANG, doc.get(F_ID_BARANG).toString())
                hm.put(F_NAMA_BARANG, doc.get(F_NAMA_BARANG).toString())
                hm.put(F_STOK, doc.get(F_STOK).toString())
                hm.put(F_HARGA, doc.get(F_HARGA).toString())
                hm.put(F_URL_GAMBAR, doc.get(F_URL_GAMBAR).toString())
                alBarang.add(hm)
            }
            adapter = CustomAdapter(this,alBarang)
            lsDataCariBarang.adapter = adapter
        }
    }
}