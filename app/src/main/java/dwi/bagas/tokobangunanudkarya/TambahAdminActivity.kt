package dwi.bagas.tokobangunanudkarya

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_tambah_admin.*

class TambahAdminActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(p0: View?) {
        var email = edRegEmail.text.toString()
        var password = edRegPassword.text.toString()

        if (email.isEmpty() || password.isEmpty()){
            Toast.makeText(this,"Email / password tidak boleh kosong", Toast.LENGTH_LONG).show()
        }else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this,"Berhasil menambah admin", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this,"Email / password salah", Toast.LENGTH_SHORT).show()

                }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_admin)
        btnRegister.setOnClickListener(this)
    }
}
