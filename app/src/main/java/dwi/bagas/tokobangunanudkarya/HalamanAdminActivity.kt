package dwi.bagas.tokobangunanudkarya

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_halaman_admin.*

class HalamanAdminActivity : AppCompatActivity(), View.OnClickListener {

    var fbAuth = FirebaseAuth.getInstance()

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahAdmin ->{
                var tambahAdmin = Intent(this, TambahAdminActivity::class.java)
                startActivity(tambahAdmin)
                true
            }

            R.id.btnDataBarang ->{
                var dataBarang = Intent(this, BarangActivity::class.java)
                startActivity(dataBarang)
                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_admin)
        btnTambahAdmin.setOnClickListener(this)
        btnDataBarang.setOnClickListener(this)

        var paket : Bundle? = intent.extras
        var teks = paket?.getString("email")
        txEmail.setText(teks)

        btnLogout.setOnClickListener {
            fbAuth.signOut()
            Toast.makeText(this,"Berhasil Logout",Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}
